import React from "react";
import "./home.css";
import ProductList from "./ProductList";
import { Dropdown } from "react-bootstrap";

const Home = () => {
  return (
    <>
      <div className="about" style={{ position: "relative" }}></div>
      <div className="container">
        <div className="row">
          <img
            src="assets\home_img\text-about.png"
            style={{ position: "absolute", bottom: "240px", width: "60%" }}
          />
          <div className="col-lg-2 mt-5">
            <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                Category
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Women</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Men</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className="col-lg-10 mt-5">
            <h5 className="">
              Showing 9 results from total 18 for "shirts" on Sale
            </h5>
            <ProductList />
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
