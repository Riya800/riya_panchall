import React from "react";
import { Card, Button } from "react-bootstrap";
import Data from "../../../data.json";
import { NavLink } from "react-router-dom";
import { useState } from "react";

const ProductList = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <div className="productList mb-5">
      <div className="container">
        <div className="row">
          {Data.map((post, key) => {
            return (
              <>
                <div className="col-lg-4">
                  <Card
                    style={{
                      border: "none",
                      boxShadow: "0px 0px 1px 2px #ddd",
                    }}
                    className="mt-5 card_style"
                    key={post.id}
                  >
                    <Card.Img
                      variant="top"
                      src={post.imgdata}
                      style={{ height: "16rem" }}
                      className="mt-3"
                    />
                    <span
                      style={{
                        position: "absolute",
                        top: "20px",
                        left: "15px",
                        backgroundColor: "red",
                        color: "white",
                        padding: "5px 14px",
                        borderRadius: "4px",
                      }}
                    >
                      {post.off}OFF
                    </span>
                    <Card.Body>
                      <div className="size-home mt-5">
                        <button
                          style={{
                            border: "3px solid #eba603",
                            padding: "2px 10px",
                          }}
                        >
                          S
                        </button>
                        <button
                          className="size-M"
                          style={{
                            marginLeft: 20,
                            border: "3px solid #eba603",
                            padding: "2px 10px",
                          }}
                        >
                          M
                        </button>
                        <button
                          className="size-L"
                          style={{
                            marginLeft: 20,
                            border: "3px solid #eba603",
                            padding: "2px 10px",
                          }}
                        >
                          L
                        </button>
                        <button
                          className="size-XL"
                          style={{
                            marginLeft: 20,
                            border: "3px solid #eba603",
                            padding: "2px 10px",
                          }}
                        >
                          XL
                        </button>
                      </div>

                      <div className="button_div d-flex mt-4">
                        <NavLink to={`/Detail/${post.id}`}>
                          <Button
                            variant="primary"
                            className="col-lg-12"
                            style={{
                              width: "100%",
                              backgroundColor: "#eba603",
                              color: "white",
                              border: "transparent",
                            }}
                            onClick={handleClick}
                          >
                            View Details
                          </Button>
                        </NavLink>

                        <i
                          className="fa-regular fa-heart"
                          style={{
                            position: "absolute",
                            right: 0,
                            fontSize: "20px",
                            marginRight: "20px",
                            color: "white",
                            borderRadius: 20,
                            padding: 8,
                            background: "#f9c603",
                          }}
                        ></i>
                      </div>
                    </Card.Body>
                  </Card>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductList;
