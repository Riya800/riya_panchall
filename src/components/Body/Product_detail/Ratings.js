import React from "react";

const Ratings = () => {
  return (
    <div className="container">
      <div className="row">
        <h3>Product Ratings & Reviews</h3>
        <div className="row">
          <div className="col-lg-6">
            <span>
              4.2<sup>*</sup>
            </span>
            <br />
            <span>1,534 ratings 498 reviews</span>
          </div>
          <div className="col-lg-6">
            <ul style={{ listStyleType: "none", padding: 0 }}>
              <li>Excellent</li>
              <li>Very Good</li>
              <li>Good</li>
              <li>Average</li>
              <li>Poor</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Ratings;
