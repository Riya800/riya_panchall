import React from "react";
import "./Detail.css";
import { Tab, Tabs } from "react-bootstrap";
import Ratings from "./Ratings";
import Data from "../../../data.json";

const Detail = () => {
  return (
    <>
      <div className="container">
        <div className="row">
          {Data.map((post, key) => {
            return (
              <>
                <div className="col-lg-6 mt-5" key={post.id}>
                  <img variant="top"
                      src={post.imgdata}
                      style={{ height: "16rem" }}
                      className="mt-3" alt="top img" />
                </div>
                <div className="col-lg-6 mt-5">
                  <h2>{post.rname}</h2>
                  <div className="rate mt-5">
                    <span>$8.00</span>
                    <span className="rate2">$6.00</span>
                  </div>
                  <h3 className="mt-5">Color</h3>
                  <div className="color-t">
                    <button className="button_black"></button>
                    <button className="button_orange"></button>
                    <button className="button_white"></button>
                  </div>

                  <div className="size mt-5">
                    <button>S</button>
                    <button className="size-M">M</button>
                    <button className="size-L">L</button>
                    <button className="size-XL">XL</button>
                  </div>

                  <div className="d-flex justify-content-between quantity">
                    <h3 className="mt-5">Quantity</h3>
                    <div className="mt-5 d-flex justify-content-between align-items-center quantity_tag">
                      <span style={{ fontSize: 24 }}>-</span>
                      <span style={{ fontSize: 22 }}>0</span>
                      <span style={{ fontSize: 24 }}>+</span>
                    </div>
                  </div>

                  <div className="mt-3 addCart">Add to Cart</div>

                  <span className="fav mt-4">
                    <i className="fa-regular fa-heart mt-1"></i>
                    <span style={{ marginLeft: "10px", fontWeight: "00" }}>
                      Favourite
                    </span>
                  </span>
                </div>
                <div className="col-lg-6 mb-5 mt-4 desc">
                  <Tabs
                    defaultActiveKey="profile"
                    id="uncontrolled-tab-example"
                    className="mb-3"
                  >
                    <Tab eventKey="home" title="Description">
                      Cop Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a
                      type specimen book. It has survived not only five
                      centuries
                      <ul
                        className="mt-4"
                        style={{ listStyleType: "none", padding: 0 }}
                      >
                        <li>
                          <i class="fa-solid fa-check"></i> 100% pima cotton
                        </li>
                        <li>
                          <i class="fa-solid fa-check"></i> Made in Peru
                        </li>
                        <li>
                          <i class="fa-solid fa-check"></i> Breathable Pima
                          cotton pique
                        </li>
                        <li>
                          <i class="fa-solid fa-check"></i> Rib knit crewneck
                        </li>
                        <li>
                          <i class="fa-solid fa-check"></i> Embroidered Bunny
                          logo at chest
                        </li>
                        <li>
                          <i class="fa-solid fa-check"></i> knit stripes at
                          cuffs
                        </li>
                      </ul>
                    </Tab>

                    <Tab eventKey="profile" title="Size & Fit">
                      Cop Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a
                      type specimen book. It has survived not only five
                      centuries
                    </Tab>
                  </Tabs>
                </div>
                <div className="col-lg-6 mt-4">{/* <Ratings/> */}</div>

                <hr />
              </>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Detail;

{
  /* <div className='col-lg-6 mt-5'>
<h2>Sleeveless Shirt</h2>
<div className='rate mt-5'>
    <span>$8.00</span>
    <span className='rate2'>$6.00</span>
</div>
<h3 className='mt-5'>Color</h3>
<div className='color-t'>
    <button className='button_black'></button>
    <button className='button_orange'></button>
    <button className='button_white'></button>
</div>
<div className='size mt-5'>
    <button>S</button>
    <button className='size-M'>M</button>
    <button className='size-L'>L</button>
    <button className='size-XL'>XL</button>
</div>
<div className='d-flex justify-content-between quantity'>
    <h3 className='mt-5'>Quantity</h3>
    <div className='mt-5 d-flex justify-content-between align-items-center quantity_tag' >
        {/* <span>Quantity</span> */
}
//         <span style={{fontSize:24}} >-</span>
//         <span style={{fontSize:22}}>0</span>
//         <span style={{fontSize:24}}>+</span>
//     </div>
// </div>

// <div className='mt-3 addCart'>Add to Cart</div>

// <span className='fav mt-4'><i className="fa-regular fa-heart mt-1"></i><span style={{marginLeft:"10px", fontWeight:"00"}}>Favourite</span></span>

// </div>
// <div className='col-lg-6 mb-5 mt-4 desc' >
// <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
//     <Tab eventKey="home" title="Description">Cop Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
//     <ul className='mt-4' style={{listStyleType:"none", padding:0}}>
//         <li><i class="fa-solid fa-check"></i> 100% pima cotton</li>
//         <li><i class="fa-solid fa-check"></i> Made in Peru</li>
//         <li><i class="fa-solid fa-check"></i> Breathable Pima cotton pique</li>
//         <li><i class="fa-solid fa-check"></i> Rib knit crewneck</li>
//         <li><i class="fa-solid fa-check"></i> Embroidered Bunny logo at chest</li>
//         <li><i class="fa-solid fa-check"></i> knit stripes at cuffs</li>
//     </ul></Tab>

//     <Tab eventKey="profile" title="Size & Fit">Cop Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</Tab>
// </Tabs>
// </div>
//<div className='col-lg-6 mt-4'>
{
  /* <Ratings/> */
}
//</div> */}
