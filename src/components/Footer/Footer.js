import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-lg-6 foot-res">
            <h2>Outfit</h2>
            <span>Discover Your Distinctive Look:</span>
            <br />
            <span>Fashioned with Precision, Warn with Confidence</span>
          </div>
          <div
            className="col-lg-6 justify-content-end page-links"
            style={{ position: "relative" }}
          >
            <ul>
              <li>Home</li>
              <li style={{ marginLeft: 20 }}>Products</li>
              <li style={{ marginLeft: 20 }}>Sale</li>
              <li style={{ marginLeft: 20 }}>Cart</li>
            </ul>
            <br />
            <ul className="social-icon mt-4">
              <li>
                <i class="fa-brands fa-facebook"></i>
              </li>
              <li>
                <i class="fa-brands fa-instagram"></i>
              </li>
              <li>
                <i class="fa-brands fa-snapchat"></i>
              </li>
            </ul>
          </div>
          <hr className="mt-5 mb-5"></hr>
          <div
            style={{ justifyContent: "space-between", display: "flex" }}
            className="mb-5"
          >
            <span>Privacy Policy</span>
            <span>&copy 2023 Outfit, Inc.</span>
            <span>Terms & Conditions</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
